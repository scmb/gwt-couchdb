package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.DocChangeEvent;

public interface IDocChangeEventHandler {
	public void onChange(DocChangeEvent data);
}
