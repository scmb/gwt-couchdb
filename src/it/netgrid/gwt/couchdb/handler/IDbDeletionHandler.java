package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.SimpleResponse;

public interface IDbDeletionHandler extends IResponseHandler<SimpleResponse> {

}
