package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.DocUpdateResponse;

public interface IDocUpdateHandler extends IResponseHandler<DocUpdateResponse> {
}