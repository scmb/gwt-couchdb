package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.DocChangeEvent;

import com.google.gwt.core.client.JsArray;

public interface IChangeCompleteHandler extends IResponseHandler<JsArray<DocChangeEvent>> {
}
