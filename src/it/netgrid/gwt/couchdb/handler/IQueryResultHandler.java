package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.QueryResponse;

import com.google.gwt.core.client.JavaScriptObject;

public interface IQueryResultHandler<D extends JavaScriptObject> extends IResponseHandler<QueryResponse<D>> {

}
