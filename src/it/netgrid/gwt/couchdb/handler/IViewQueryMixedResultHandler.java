package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.MixedQueryResponse;

public interface IViewQueryMixedResultHandler extends IResponseHandler<MixedQueryResponse> {

}
