package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.DbInfoResponse;

public interface IDbInfoHandler extends IResponseHandler<DbInfoResponse> {
}