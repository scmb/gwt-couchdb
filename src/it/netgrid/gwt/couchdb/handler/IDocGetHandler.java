package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.CouchDbDoc;

public interface IDocGetHandler<D extends CouchDbDoc> extends IResponseHandler<D> {

}
