package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.ActiveTask;

import com.google.gwt.core.client.JsArray;

public interface IActiveTasksHandler extends IResponseHandler<JsArray<ActiveTask>> {
}
