package it.netgrid.gwt.couchdb.handler;

import com.google.gwt.core.client.JsArrayString;

public interface IAllDbsHandler extends IResponseHandler<JsArrayString> {
}
