package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.ErrorResponse;

public interface IResponseHandler<R> {

	public void onSuccess(R response);
	
	public void onError(ErrorResponse error);
}
