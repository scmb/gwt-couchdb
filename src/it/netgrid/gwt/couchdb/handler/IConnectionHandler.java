package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.ICouchDb;

public interface IConnectionHandler extends IResponseHandler<ICouchDb> {

}
