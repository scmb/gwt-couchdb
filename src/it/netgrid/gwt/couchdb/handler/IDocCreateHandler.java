package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.DocCreateResponse;

public interface IDocCreateHandler extends IResponseHandler<DocCreateResponse> {

}
