package it.netgrid.gwt.couchdb.handler;

import it.netgrid.gwt.couchdb.response.ReplicationResult;

public interface IReplicationCompleteHandler extends IResponseHandler<ReplicationResult> {
}
