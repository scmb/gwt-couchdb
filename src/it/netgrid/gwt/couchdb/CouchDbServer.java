package it.netgrid.gwt.couchdb;

import java.util.ArrayList;
import java.util.List;

import it.netgrid.gwt.couchdb.handler.IActiveTasksHandler;
import it.netgrid.gwt.couchdb.handler.IAllDbsHandler;
import it.netgrid.gwt.couchdb.handler.ICompactionHandler;
import it.netgrid.gwt.couchdb.handler.IConnectionHandler;
import it.netgrid.gwt.couchdb.handler.IDbCreationHandler;
import it.netgrid.gwt.couchdb.handler.IDbDeletionHandler;
import it.netgrid.gwt.couchdb.handler.IDbInfoHandler;
import it.netgrid.gwt.couchdb.handler.IDocCreateHandler;
import it.netgrid.gwt.couchdb.handler.IDocGetHandler;
import it.netgrid.gwt.couchdb.handler.IDocUpdateHandler;
import it.netgrid.gwt.couchdb.handler.IResponseHandler;
import it.netgrid.gwt.couchdb.handler.IViewQueryMixedResultHandler;
import it.netgrid.gwt.couchdb.handler.IViewQueryResultHandler;
import it.netgrid.gwt.couchdb.options.ChangesOptions;
import it.netgrid.gwt.couchdb.options.GetDocOptions;
import it.netgrid.gwt.couchdb.options.ViewQueryOptions;
import it.netgrid.gwt.couchdb.response.ErrorResponse;
import it.netgrid.gwt.couchdb.response.QueryResponse;
import it.netgrid.gwt.couchdb.response.SimpleResponse;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestBuilder.Method;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

public class CouchDbServer {
	
	private static CouchDbServer singleton;
	
	public class CouchDbImpl implements ICouchDb {
		
		protected IConnectionConfig config;
		
		protected CouchDbImpl(IConnectionConfig config) {
			this.config = config;
		}
		
		public void compact(ICompactionHandler handler) {
			UrlBuilder url = initBuilder(this.config);
			Path path = initPath(config);
			path.add("_compact");
			url.setPath(path.buildPath());
			sendObjectRequest(RequestBuilder.POST, url, null, handler);
		}
		
		public IChangesFeed changes(ChangesOptions options) {
			return new ChangesFeedImpl(config, options);
		}

		@Override
		public void get(String id, IDocGetHandler<?> callback) {
			this.get(id, callback, null);
		}

		@Override
		public void get(String id, IDocGetHandler<?> callback, GetDocOptions options) {
			UrlBuilder url = initBuilder(this.config);
			Path path = initPath(config);
			path.add(id);
			url.setPath(path.buildPath());
			sendObjectRequest(RequestBuilder.GET, url, options, callback);
		}

		@Override
		public void info(IDbInfoHandler callback) {
			UrlBuilder url = initBuilder(this.config);
			Path path = initPath(config);
			url.setPath(path.buildPath());
			sendObjectRequest(RequestBuilder.GET, url, null, callback);
		}

		@Override
		public void post(CouchDbDoc doc, IDocCreateHandler callback) {
			UrlBuilder url = initBuilder(this.config);
			Path path = initPath(config);
			url.setPath(path.buildPath());
			sendObjectRequest(RequestBuilder.POST, url, doc, null, callback);
		}

		@Override
		public void put(CouchDbDoc doc, IDocUpdateHandler callback) {
			UrlBuilder url = initBuilder(this.config);
			Path path = initPath(config);
			path.add(doc.getId());
			url.setPath(path.buildPath());
			sendObjectRequest(RequestBuilder.PUT, url, doc, null, callback);
		}

		@Override
		public void delete(CouchDbDoc doc, IDocUpdateHandler callback) {
			UrlBuilder url = initBuilder(this.config);
			Path path = initPath(config);
			path.add(doc.getId());
			url.setParameter("rev", doc.getRev());
			url.setPath(path.buildPath());
			sendObjectRequest(RequestBuilder.DELETE, url, null, callback);
		}

		@Override
		public void queryView(String view, ViewQueryOptions options, IViewQueryResultHandler<?> callback) {
			UrlBuilder url = initBuilder(this.config);
			Path path = initPath(config);
			Path viewPath = Path.parse(view);
			path.add("_design");
			path.add(viewPath.get(0));
			path.add("_view");
			path.add(viewPath.get(1));
			url.setPath(path.buildPath());
			sendObjectRequest(RequestBuilder.GET, url, options, callback);
		}

		@Override
		public void queryView(String view, IViewQueryResultHandler<?> callback) {
			this.queryView(view, null, callback);
		}

		@Override
		public void queryView(String view, IViewQueryMixedResultHandler callback) {
			this.queryView(view, null, callback);
		}

		@Override
		public void queryView(String view, ViewQueryOptions options, IViewQueryMixedResultHandler callback) {
			UrlBuilder url = initBuilder(this.config);
			Path path = initPath(config);
			Path viewPath = Path.parse(view);
			path.add("_design");
			path.add(viewPath.get(0));
			path.add("_view");
			path.add(viewPath.get(1));
			url.setPath(path.buildPath());
			
			Method method = options.getKeys() == null ? RequestBuilder.GET : RequestBuilder.POST;
			
			sendObjectRequest(method, url, options, callback);
		}

		@Override
		public <D extends JavaScriptObject> List<D> responseToList(QueryResponse<D> response) {
			return CouchDbServer.toList(response);
		}

		@Override
		public <D extends JavaScriptObject> List<D> arrayToList(JsArray<D> array) {
			return CouchDbServer.arrayToList(array);
		}

	}
	
	public static void connect(IConnectionConfig config, IConnectionHandler handler) {
		handler.onSuccess(CouchDbServer.get().new CouchDbImpl(config));
	}
	
	public static void create(IConnectionConfig config, IDbCreationHandler handler) {
		UrlBuilder url = initBuilder(config);
		Path path = initPath(config);
		url.setPath(path.buildPath());
		sendObjectRequest(RequestBuilder.PUT, url, null, handler);
	}
	
	public static void delete(IConnectionConfig config, IDbDeletionHandler handler) {
		UrlBuilder url = initBuilder(config);
		Path path = initPath(config);
		url.setPath(path.buildPath());
		sendObjectRequest(RequestBuilder.DELETE, url, null, handler);
	}
	
	public static void connectOrCreate(final IConnectionConfig config, final IConnectionHandler handler) {
		allDbs(config, new IAllDbsHandler() {
			
			@Override
			public void onSuccess(JsArrayString response) {
				// Verifico presenza DB
				for(int i = 0; i < response.length(); i++) {
					if(response.get(i) != null && response.get(i).equals(config.getDbName())) {
						handler.onSuccess(CouchDbServer.get().new CouchDbImpl(config));
						return;
					}
				}
				
				// Creo DB
				CouchDbServer.create(config, new IDbCreationHandler() {
					
					@Override
					public void onSuccess(SimpleResponse response) {
						handler.onSuccess(CouchDbServer.get().new CouchDbImpl(config));
					}
					
					@Override
					public void onError(ErrorResponse error) {
						handler.onError(error);
					}
				});
			}
			
			@Override
			public void onError(ErrorResponse error) {
				handler.onError(error);
			}
		});
	}
	
	public static <D extends JavaScriptObject> List<D> responseToList(QueryResponse<D> response) {
		return CouchDbServer.toList(response);
	}
	
	public static <D extends JavaScriptObject> List<D> toList(QueryResponse<D> response) {
		ArrayList<D> retval = new ArrayList<D>();
		
		if(response == null) return retval;
		
		for(int i = 0; i<response.getRows().length(); i++) {
			retval.add(response.getRows().get(i).getValue());
		}
		
		return retval;		
	}
	
	public static final <D extends JavaScriptObject> List<D> toList(JsArray<D> array) {
		ArrayList<D> retval = new ArrayList<D>();
		
		if(array == null) return retval;
		
		for(int i = 0; i<array.length(); i++) {
			retval.add(array.get(i));
		}
		
		return retval;
	}

	public static <D extends JavaScriptObject> List<D> arrayToList(JsArray<D> array) {
		return CouchDbServer.toList(array);
	}

	public static void activeTasks(IConnectionConfig config, IActiveTasksHandler handler) {
		UrlBuilder url = initBuilder(config);
		url.setPath("_active_tasks");
		sendObjectRequest(RequestBuilder.GET, url, null, handler);
	}
	
	public static void allDbs(IConnectionConfig config, IAllDbsHandler handler) {
		UrlBuilder url = initBuilder(config);
		url.setPath("_all_dbs");
		sendArrayRequest(RequestBuilder.GET, url, null, handler);
	}
	
	private static CouchDbServer get() {
		if(singleton == null) {
			singleton = new CouchDbServer();
		}
		
		return singleton;
	}
	
	private static <R> void sendArrayRequest(Method method, UrlBuilder urlBuilder, JavaScriptObject options, IResponseHandler<R> handler) {
		sendArrayRequest(method, urlBuilder, null, options, handler);
	}
	
	private static <R> void sendArrayRequest(Method method, UrlBuilder urlBuilder, CouchDbDoc data, JavaScriptObject options, IResponseHandler<R> handler) {
		HttpUtils.loadParams(urlBuilder, options);		
		RequestBuilder builder = new RequestBuilder(method, urlBuilder.buildString());
		builder.setHeader("Accept", "application/json");
		builder.setHeader("Content-Type", "application/json; charset=UTF-8");
		
		//if(method == RequestBuilder.GET || method == RequestBuilder.HEAD)
		//	builder.setHeader("Cache-Control", "must-revalidate");
		
		//builder.setHeader("Cache-Control", "max-age=0, no-cache");
		builder.setCallback(buildArrayCallback(handler));
		
		if(data != null) {
			HttpUtils.loadData(builder, data);
		}
		
		try {
			builder.send();
		} catch (RequestException e) {
			ErrorResponse error = JavaScriptObject.createObject().cast();
			error.setStatus(Response.SC_SERVICE_UNAVAILABLE);
			error.setError("Exception");
			error.setReason(e.getMessage());
			handler.onError(error);
		}
	}
	
	private static <R> void sendObjectRequest(Method method, UrlBuilder urlBuilder, JavaScriptObject options, IResponseHandler<R> handler) {
		sendObjectRequest(method, urlBuilder, null, options, handler);
	}
	
	private static <R> void sendObjectRequest(Method method, UrlBuilder urlBuilder, CouchDbDoc data, JavaScriptObject options, IResponseHandler<R> handler) {
		HttpUtils.loadParams(urlBuilder, options);		
		RequestBuilder builder = new RequestBuilder(method, urlBuilder.buildString());
		builder.setHeader("Accept", "application/json");
		builder.setHeader("Content-Type", "application/json; charset=UTF-8");
		
		//if(method == RequestBuilder.GET || method == RequestBuilder.HEAD)
		//	builder.setHeader("Cache-Control", "must-revalidate");
		
		builder.setCallback(buildObjectCallback(handler));
		
		if(data != null) {
			HttpUtils.loadData(builder, data);
		}
		
		try {
			builder.send();
		} catch (RequestException e) {
			ErrorResponse error = JavaScriptObject.createObject().cast();
			error.setStatus(Response.SC_SERVICE_UNAVAILABLE);
			error.setError("Exception");
			error.setReason(e.getMessage());
			handler.onError(error);
		}
	}
	
	private static <R> RequestCallback buildArrayCallback(final IResponseHandler<R> handler) {
		return new RequestCallback() {
			
			@Override
			public void onResponseReceived(Request request, Response response) {
				switch(response.getStatusCode()) {
				case Response.SC_ACCEPTED:
				case Response.SC_OK:
				case Response.SC_NOT_MODIFIED:
				case Response.SC_CONFLICT:
				case Response.SC_CREATED:
				case Response.SC_NO_CONTENT:
					// Le operazioni per questo genere di risultati verranno
					// eseguite DOPO lo switch che di default ritorna prima
					break;
					
// 				Commentati per evitare di fare troppi confronti ad ogni richiesta
//				Rimangono come indicazione degli altri possibili valori
//
//				case Response.SC_BAD_GATEWAY:
//				case Response.SC_BAD_REQUEST:
//				case Response.SC_CONFLICT:
//				case Response.SC_CONTINUE:
//				case Response.SC_CREATED:
//				case Response.SC_EXPECTATION_FAILED:
//				case Response.SC_FORBIDDEN:
//				case Response.SC_GATEWAY_TIMEOUT:
//				case Response.SC_GONE:
//				case Response.SC_HTTP_VERSION_NOT_SUPPORTED:
//				case Response.SC_INTERNAL_SERVER_ERROR:
//				case Response.SC_LENGTH_REQUIRED:
//				case Response.SC_METHOD_NOT_ALLOWED:
//				case Response.SC_MOVED_PERMANENTLY:
//				case Response.SC_MOVED_TEMPORARILY:
//				case Response.SC_MULTIPLE_CHOICES:
//				case Response.SC_NO_CONTENT:
//				case Response.SC_NON_AUTHORITATIVE_INFORMATION:
//				case Response.SC_NOT_ACCEPTABLE:
//				case Response.SC_NOT_FOUND:
//				case Response.SC_NOT_IMPLEMENTED:
//				case Response.SC_PARTIAL_CONTENT:
//				case Response.SC_PAYMENT_REQUIRED:
//				case Response.SC_PRECONDITION_FAILED:
//				case Response.SC_PROXY_AUTHENTICATION_REQUIRED:
//				case Response.SC_REQUEST_ENTITY_TOO_LARGE:
//				case Response.SC_REQUESTED_RANGE_NOT_SATISFIABLE:
//				case Response.SC_RESET_CONTENT:
//				case Response.SC_SEE_OTHER:
//				case Response.SC_SERVICE_UNAVAILABLE:
//				case Response.SC_SWITCHING_PROTOCOLS:
//				case Response.SC_TEMPORARY_REDIRECT:
//				case Response.SC_UNAUTHORIZED:
//				case Response.SC_UNSUPPORTED_MEDIA_TYPE:
//				case Response.SC_USE_PROXY:
				default:
					ErrorResponse error = JavaScriptObject.createObject().cast();
					error.setStatus(response.getStatusCode());
					error.setError("Error " + response.getStatusCode());
					error.setReason(response.getStatusText());
					handler.onError(error);
					return;
				}

				String responseText = response.getText();
				JsArrayString jsonData = toJsArrayString(responseText);
				if(jsonData == null) {
					ErrorResponse error = JavaScriptObject.createObject().cast();
					error.setStatus(response.getStatusCode());
					error.setError("Empty response with status " + response.getStatusCode());
					error.setReason(response.getStatusText());
					handler.onError(error);
					return;			
				}
				
				if(isDbError(jsonData)) {
					ErrorResponse error = jsonData.cast();
					handler.onError(error);
					return;
				}
				
				R payload = cast(jsonData);
				handler.onSuccess(payload);
			}
			
			@Override
			public void onError(Request request, Throwable exception) {
				ErrorResponse error = JavaScriptObject.createObject().cast();
				error.setStatus(Response.SC_SERVICE_UNAVAILABLE);
				error.setError("Exception");
				error.setReason(exception.getMessage());
				
				handler.onError(error);
			}
		};
	}
	
	private static <R> RequestCallback buildObjectCallback(final IResponseHandler<R> handler) {
		return new RequestCallback() {
			
			@Override
			public void onResponseReceived(Request request, Response response) {
				switch(response.getStatusCode()) {
				case Response.SC_ACCEPTED:
				case Response.SC_OK:
				case Response.SC_NOT_MODIFIED:
				case Response.SC_CONFLICT:
				case Response.SC_CREATED:
				case Response.SC_NO_CONTENT:
					// Le operazioni per questo genere di risultati verranno
					// eseguite DOPO lo switch che di default ritorna prima
					break;
					
// 				Commentati per evitare di fare troppi confronti ad ogni richiesta
//				Rimangono come indicazione degli altri possibili valori
//
//				case Response.SC_BAD_GATEWAY:
//				case Response.SC_BAD_REQUEST:
//				case Response.SC_CONTINUE:
//				case Response.SC_EXPECTATION_FAILED:
//				case Response.SC_FORBIDDEN:
//				case Response.SC_GATEWAY_TIMEOUT:
//				case Response.SC_GONE:
//				case Response.SC_HTTP_VERSION_NOT_SUPPORTED:
//				case Response.SC_INTERNAL_SERVER_ERROR:
//				case Response.SC_LENGTH_REQUIRED:
//				case Response.SC_METHOD_NOT_ALLOWED:
//				case Response.SC_MOVED_PERMANENTLY:
//				case Response.SC_MOVED_TEMPORARILY:
//				case Response.SC_MULTIPLE_CHOICES:
//				case Response.SC_NO_CONTENT:
//				case Response.SC_NON_AUTHORITATIVE_INFORMATION:
//				case Response.SC_NOT_ACCEPTABLE:
//				case Response.SC_NOT_FOUND:
//				case Response.SC_NOT_IMPLEMENTED:
//				case Response.SC_PARTIAL_CONTENT:
//				case Response.SC_PAYMENT_REQUIRED:
//				case Response.SC_PRECONDITION_FAILED:
//				case Response.SC_PROXY_AUTHENTICATION_REQUIRED:
//				case Response.SC_REQUEST_ENTITY_TOO_LARGE:
//				case Response.SC_REQUESTED_RANGE_NOT_SATISFIABLE:
//				case Response.SC_RESET_CONTENT:
//				case Response.SC_SEE_OTHER:
//				case Response.SC_SERVICE_UNAVAILABLE:
//				case Response.SC_SWITCHING_PROTOCOLS:
//				case Response.SC_TEMPORARY_REDIRECT:
//				case Response.SC_UNAUTHORIZED:
//				case Response.SC_UNSUPPORTED_MEDIA_TYPE:
//				case Response.SC_USE_PROXY:
				default:
					ErrorResponse error = JavaScriptObject.createObject().cast();
					error.setStatus(response.getStatusCode());
					error.setError("Error " + response.getStatusCode());
					error.setReason(response.getStatusText());
					handler.onError(error);
					return;
				}
				
				String responseText = response.getText();
				JavaScriptObject jsonData = toJsObject(responseText);
				if(jsonData == null) {
					ErrorResponse error = JavaScriptObject.createObject().cast();
					error.setStatus(response.getStatusCode());
					error.setError("Empty response with status " + response.getStatusCode());
					error.setReason(response.getStatusText());
					handler.onError(error);
					return;			
				}
				
				if(isDbError(jsonData)) {
					ErrorResponse error = jsonData.cast();
					handler.onError(error);
					return;
				}
				
				R payload = cast(jsonData);
				handler.onSuccess(payload);
			}
			
			@Override
			public void onError(Request request, Throwable exception) {
				ErrorResponse error = JavaScriptObject.createObject().cast();
				error.setStatus(Response.SC_SERVICE_UNAVAILABLE);
				error.setError("Exception");
				error.setReason(exception.getMessage());
				
				handler.onError(error);
			}
		};
	}
	
	private static final native <R> R cast(JavaScriptObject obj) /*-{
		return obj;
	}-*/;

	private static final JavaScriptObject toJsObject(String text) {
		if(text == null || text.equals("")) return null;
		
		JSONValue value = JSONParser.parseStrict(text);
		return value.isObject() == null ? null : value.isObject().getJavaScriptObject();
	}
	
	private static final JsArrayString toJsArrayString(String text) {
		JsArrayString retval = emptyJsArrayString();
		if(text == null || text.equals("")) return retval;
		
		JSONValue value = JSONParser.parseStrict(text);
		JSONArray array = value.isArray();
		if(array == null) return retval;
		
		for(int i=0; i<array.size(); i++) {
			retval.push(array.get(i).isString().stringValue());
		}
		
		return retval;
	}
	
	private static native JsArrayString emptyJsArrayString() /*-{
		return [];
	}-*/;


	private static final native boolean isDbError(JavaScriptObject obj) /*-{
		if(obj == null) return true;
		
		if(typeof obj._id != "undefined")
			return false;
		else 
			return typeof obj.error != "undefined";
	}-*/;
	
//	public static void checkConnection(IConnectionConfig config, RequestCallback callback) {
//		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, connString(config));
//		builder.setCallback(callback);
//		
//		try {
//			builder.send();
//		} catch (RequestException e) {
//			e.printStackTrace();
//		}
//	}
	
	private static UrlBuilder initBuilder(IConnectionConfig config) {
		UrlBuilder builder = new UrlBuilder();
		
		builder.setProtocol(config.getProtocol().name());
		builder.setHost(config.getHostname());
		builder.setPort(config.getPort());
		
		return builder;
	}
	
	private static Path initPath(IConnectionConfig config) {
		Path path = new Path();
		path.add(config.getDbName());
		return path;
	}
	
	private static String connString(IConnectionConfig config) {
		UrlBuilder builder = new UrlBuilder();
		
		builder.setProtocol(config.getProtocol().name());
		builder.setHost(config.getHostname());
		builder.setPort(config.getPort());
		builder.setPath(config.getDbName());
		
		return builder.buildString();
	}
	
	private static final native String encodeDocId(String docID) /*-{
		var parts = docID.split("/");
		if (parts[0] == "_design") {
			parts.shift();
			return "_design/" + encodeURIComponent(parts.join('/'));
		}
		return encodeURIComponent(docID);
	}-*/;

}
