package it.netgrid.gwt.couchdb.response;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public final class ChangesResponse extends JavaScriptObject {
	protected ChangesResponse() {}
	
	public native JsArray<DocChangeEvent> getResults() /*-{
		return this.results;
	}-*/;

	public native int getLastSeq() /*-{
		return this.last_seq;
	}-*/;

}
