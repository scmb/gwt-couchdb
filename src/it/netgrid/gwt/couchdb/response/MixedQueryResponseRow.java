package it.netgrid.gwt.couchdb.response;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayMixed;

public class MixedQueryResponseRow extends JavaScriptObject {

	protected MixedQueryResponseRow() {
	}

	public final native int getKeyInt() /*-{
		return this.key;
	}-*/;

	public final native JavaScriptObject getKeyObject() /*-{
		return this.key;
	}-*/;

	public final native String getKeyString() /*-{
		return this.key;
	}-*/;

	public final native boolean getKeyBoolean() /*-{
		return this.key;
	}-*/;

	public final native JsArrayMixed getKeyArray() /*-{
		return this.key;
	}-*/;

	public final native int getValueInt() /*-{
		return this.value;
	}-*/;

	public final native JavaScriptObject getValueObject() /*-{
		return this.value;
	}-*/;

	public final native String getValueString() /*-{
		return this.value;
	}-*/;

	public final native boolean getValueBoolean() /*-{
		return this.value;
	}-*/;

	public final native JsArrayMixed getValueArray() /*-{
		return this.value;
	}-*/;

}
