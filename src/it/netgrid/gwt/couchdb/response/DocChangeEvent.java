package it.netgrid.gwt.couchdb.response;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class DocChangeEvent extends JavaScriptObject {
	protected DocChangeEvent() {
	}

	public final native String getId() /*-{
		return this.id;
	}-*/;

	public final native int getSeq() /*-{
		return this.seq;
	}-*/;

	public final native JsArray<RevisionReference> getChanges() /*-{
		return this.changes;
	}-*/;

	public final native JavaScriptObject getDoc() /*-{
		return this.doc;
	}-*/;

	public final native RevisionReference getValue() /*-{
		return this.value;
	}-*/;
	
	public final native boolean isDeleted() /*-{
		return this.deleted;
	}-*/;

}
