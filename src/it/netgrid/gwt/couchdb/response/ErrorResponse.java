package it.netgrid.gwt.couchdb.response;

import com.google.gwt.core.client.JavaScriptObject;

public class ErrorResponse extends JavaScriptObject {
	protected ErrorResponse() {}
	
	public final native void setError(String error) /*-{
		this.error = error;
	}-*/;

	public final native void setReason(String reason) /*-{
		this.reason = reason;
	}-*/;

	public final native void setStatus(int status) /*-{
		this.status = status;
	}-*/;
	
	public final native String getError() /*-{
		return this.error;
	}-*/;

	public final native String getReason() /*-{
		return this.reason;
	}-*/;

	public final native int getStatus()/*-{
		return this.status;
	}-*/;

}
