package it.netgrid.gwt.couchdb.response;

import com.google.gwt.core.client.JavaScriptObject;

public class SimpleResponse extends JavaScriptObject {
	protected SimpleResponse() {}
	
	public native final boolean isOk() /*-{
		return this.ok;
	}-*/;

	public native final void setOk(boolean ok) /*-{
		this.ok = ok;
	}-*/;

}
