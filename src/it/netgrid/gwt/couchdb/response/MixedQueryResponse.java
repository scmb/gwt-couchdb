package it.netgrid.gwt.couchdb.response;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class MixedQueryResponse extends JavaScriptObject {
	protected MixedQueryResponse() {}
	
	public final native JsArray<MixedQueryResponseRow> getRows() /*-{
		return this.rows;
	}-*/;
}
