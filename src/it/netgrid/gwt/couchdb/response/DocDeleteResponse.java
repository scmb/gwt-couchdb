package it.netgrid.gwt.couchdb.response;

import com.google.gwt.core.client.JavaScriptObject;

public final class DocDeleteResponse extends JavaScriptObject {
	protected DocDeleteResponse() {}
	
	public native void setOk(boolean ok) /*-{
		this.ok = ok;
	}-*/;

	public native boolean isOk() /*-{
		return this.ok;
	}-*/;

	public native void setRev(String rev) /*-{
		this.rev = rev;
	}-*/;

	public native String getRev() /*-{
		return this.rev;
	}-*/;
}
