package it.netgrid.gwt.couchdb.options;

import com.google.gwt.core.client.JavaScriptObject;

public class FilterQueryParams extends JavaScriptObject {
	protected FilterQueryParams() {}
	
	public final native void addParam(String key, String value) /*-{
		this[key] = value;
	}-*/;
	
	public final native boolean hasParam(String key) /*-{
		return "undefined" != typeof this[key];
	}-*/;
	
	public final native String getParam(String key) /*-{
		return "undefined" != typeof this[key] ? this[key] : null;
	}-*/;


}
