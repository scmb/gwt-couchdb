package it.netgrid.gwt.couchdb.options;

import it.netgrid.gwt.couchdb.CouchDbDoc;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayMixed;

public abstract class AQueryMixed {

	protected final native static void emit(String key, JavaScriptObject value) /*-{
		emit(key, value);
	}-*/;

	protected CouchDbDoc getDoc(int index, JsArrayMixed array) {
		JavaScriptObject item = array.getObject(index);
		return item == null ? (CouchDbDoc) null : item.<CouchDbDoc> cast();
	}

	protected <T extends JavaScriptObject> T getItem(int index,
			JsArrayMixed array) {
		JavaScriptObject item = array.getObject(index);
		return item == null ? (T) null : item.<T> cast();
	}

	public abstract ReductionType getReductionType();

	public abstract void map(CouchDbDoc doc);

	public abstract void reduce(JsArrayMixed keys, JsArrayMixed values,
			boolean rereduce);
}
