package it.netgrid.gwt.couchdb.options;

import it.netgrid.gwt.couchdb.CouchDbDoc;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public abstract class AQuery<D extends JavaScriptObject,K extends JavaScriptObject,V extends JavaScriptObject> {

	protected final native static void emit(String key, JavaScriptObject value) /*-{
		emit(key, value);
	}-*/;

	protected D getDoc(CouchDbDoc doc) {
		return doc.<D>cast();
	}

	public abstract ReductionType getReductionType();

	public abstract void map(CouchDbDoc doc);

	public abstract void reduce(JsArray<K> keys, JsArray<V> values,
			boolean rereduce);
}
