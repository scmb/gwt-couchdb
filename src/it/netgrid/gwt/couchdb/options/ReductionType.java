package it.netgrid.gwt.couchdb.options;

public enum ReductionType {
	NONE,
	SUM,
	COUNT,
	STATS,
	CUSTOM;
}
