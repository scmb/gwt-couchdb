package it.netgrid.gwt.couchdb.options;

public class EmptyOptionsFactory implements ICouchdbOptionsFactory {

	@Override
	public final native ViewQueryOptions buildViewQueryOptions() /*-{
		return {};
	}-*/;

	@Override
	public final native ChangesOptions buildChangesOptions() /*-{
		return {};
	}-*/;

	@Override
	public final native GetDocOptions buildGetDocOptions() /*-{
		return {};
	}-*/;

	@Override
	public final native ReplicationOptions buildReplicationOptions() /*-{
		return {};
	}-*/;


	@Override
	public final native FilterQueryParams buildFilterQueryParams() /*-{
	return {};
}-*/;

}
