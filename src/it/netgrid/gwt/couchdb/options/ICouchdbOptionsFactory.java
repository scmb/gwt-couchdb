package it.netgrid.gwt.couchdb.options;

public interface ICouchdbOptionsFactory {
	
	public ChangesOptions buildChangesOptions();

	public GetDocOptions buildGetDocOptions();
	
	public ReplicationOptions buildReplicationOptions();

	public ViewQueryOptions buildViewQueryOptions();
	
	public FilterQueryParams buildFilterQueryParams();
}
