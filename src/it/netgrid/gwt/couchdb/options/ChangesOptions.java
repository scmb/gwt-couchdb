package it.netgrid.gwt.couchdb.options;

import com.google.gwt.core.client.JavaScriptObject;

public class ChangesOptions extends JavaScriptObject {
	protected ChangesOptions() {}
	
	public final native boolean isIncludeDocs() /*-{
		return this.include_docs == true;
	}-*/;
	
	public final native void setIncludeDocs(boolean includeDocs) /*-{
		this.include_docs = includeDocs;
	}-*/;
	
	public final native boolean isConflicts() /*-{
		return this.conflicts == true;
	}-*/;
	
	public final native void setQueryParams(FilterQueryParams params) /*-{
		this.query_params = params;
	}-*/;
	
	public final native void setConflicts(boolean conflicts) /*-{
		this.conflicts = conflicts;
	}-*/;
	
	public final native boolean isDescending() /*-{
		return this.descending == true;
	}-*/;
	
	public final native void setDescending(boolean descending) /*-{
		this.descending = descending;
	}-*/;
	
	public final native String getFilter() /*-{
		return this.filter;
	}-*/;
	
	public final native void setFilter(String filter) /*-{
		this.filter = filter;
	}-*/;
	
	public final native int getSince() /*-{
		return typeof this.since == "undefined" ? 0 : this.since;
	}-*/;
	
	public final native void setSince(int since) /*-{
		this.since = since;
	}-*/;
	
	public final native void setSinceNow() /*-{
	    this.since = "now";
	}-*/;
	
	public final native boolean isSinceNow() /*-{
		return this.since == "now";
	}-*/;
	
	public final native void setLimit(int limit) /*-{
		this.limit = limit;
	}-*/;

	public final native int getLimit() /*-{
		return typeof this.limit == "undefined"  ? 0 : this.limit;
	}-*/;
	
	public final native boolean isContinuous() /*-{
		return typeof this.feed != "undefined" && this.feed == "longpoll";
	}-*/;
	
	public final native void setContinuous(boolean continuous) /*-{
		if(continuous)
			this.feed = "longpoll";
		else
			this.feed = "normal";
	}-*/;
	
	public final native void setFeed(String feedType) /*-{
		this.feed = feedType;
	}-*/;

	public final native String getFeed() /*-{
		return this.feed;
	}-*/;
	
	public final native void setHeartbeat(int millis) /*-{
		this.heartbeat = millis;
	}-*/;

	public final native int getHeartbeat() /*-{
		return this.heartbeat;
	}-*/;

	public final native void setTimeout(int millis) /*-{
		this.timeout = millis;
	}-*/;

	public final native int getTimeout() /*-{
		return this.timeout;
	}-*/;
}
