package it.netgrid.gwt.couchdb;

import it.netgrid.gwt.couchdb.handler.IChangeCompleteHandler;
import it.netgrid.gwt.couchdb.handler.IDocChangeEventHandler;

public interface IChangesFeed {

	public void start();
	public void cancel();
	
	public void addChangeListener(IDocChangeEventHandler handler);
	public void removeChangeListener(IDocChangeEventHandler handler);
	
	public void addCompleteListener(IChangeCompleteHandler handler);
	public void removeCompleteListener(IChangeCompleteHandler handler);
	
}
