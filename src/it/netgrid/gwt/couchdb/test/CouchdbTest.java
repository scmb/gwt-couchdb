package it.netgrid.gwt.couchdb.test;

import java.util.logging.Logger;

import it.netgrid.gwt.couchdb.CouchDbServer;
import it.netgrid.gwt.couchdb.IChangesFeed;
import it.netgrid.gwt.couchdb.IConnectionConfig;
import it.netgrid.gwt.couchdb.ICouchDb;
import it.netgrid.gwt.couchdb.handler.IConnectionHandler;
import it.netgrid.gwt.couchdb.handler.IDocChangeEventHandler;
import it.netgrid.gwt.couchdb.options.ChangesOptions;
import it.netgrid.gwt.couchdb.options.EmptyOptionsFactory;
import it.netgrid.gwt.couchdb.options.ICouchdbOptionsFactory;
import it.netgrid.gwt.couchdb.response.DocChangeEvent;
import it.netgrid.gwt.couchdb.response.ErrorResponse;

import com.google.gwt.core.client.EntryPoint;

public class CouchdbTest implements EntryPoint {

	private ICouchDb db;
	private Logger log;
	
	private ICouchdbOptionsFactory optFactory;
	
	@Override
	public void onModuleLoad() {
		this.optFactory = new EmptyOptionsFactory();
		this.log = Logger.getLogger("CouchdbTest");
		
		CouchDbServer.connectOrCreate(buildConfig(), new IConnectionHandler() {
			
			@Override
			public void onSuccess(ICouchDb response) {
				db = response;
				log.info("Db connected");
				startChangeListening();
			}
			
			@Override
			public void onError(ErrorResponse error) {
				log.warning(error.getReason());
			}
		});
	}
	
	private void startChangeListening() {
		ChangesOptions options = this.optFactory.buildChangesOptions();
		options.setContinuous(true);
		options.setSinceNow();
		final IChangesFeed feed = this.db.changes(options);
		feed.addChangeListener(new IDocChangeEventHandler() {
			
			@Override
			public void onChange(DocChangeEvent data) {
				log.info("Doc change triggered: " + data.getId());
				//feed.cancel();
			}
			
		});
		
		feed.start();
	}
	
	private IConnectionConfig buildConfig() {
		return new IConnectionConfig() {

			@Override
			public String getHostname() {
				return "localhost";
			}

			@Override
			public int getPort() {
				return 5984;
			}

			@Override
			public String getDbName() {
				return "gwtcouchtest";
			}

			@Override
			public Protocol getProtocol() {
				return Protocol.HTTP;
			}
			
		};
	}

}
