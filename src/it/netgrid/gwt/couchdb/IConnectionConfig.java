package it.netgrid.gwt.couchdb;

public interface IConnectionConfig {
	
	public enum Protocol {
		HTTP,
		HTTPS;
	}
	
	public String getHostname();
	
	public int getPort();
	
	public String getDbName();
	
	public Protocol getProtocol();
}
