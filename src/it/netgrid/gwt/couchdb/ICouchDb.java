package it.netgrid.gwt.couchdb;

import it.netgrid.gwt.couchdb.CouchDbDoc;
import it.netgrid.gwt.couchdb.handler.ICompactionHandler;
import it.netgrid.gwt.couchdb.handler.IDbInfoHandler;
import it.netgrid.gwt.couchdb.handler.IDocCreateHandler;
import it.netgrid.gwt.couchdb.handler.IDocGetHandler;
import it.netgrid.gwt.couchdb.handler.IDocUpdateHandler;
import it.netgrid.gwt.couchdb.handler.IViewQueryMixedResultHandler;
import it.netgrid.gwt.couchdb.handler.IViewQueryResultHandler;
import it.netgrid.gwt.couchdb.options.ChangesOptions;
import it.netgrid.gwt.couchdb.options.GetDocOptions;
import it.netgrid.gwt.couchdb.options.ViewQueryOptions;
import it.netgrid.gwt.couchdb.response.QueryResponse;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public interface ICouchDb {
//	public void allDocs(IAllDocsHandler callback);
//
//	public void allDocs(IAllDocsHandler callback, AllDocsOptions options);

//	public <D extends CouchDbDoc> void bulkDocs(JsArray<D> docs);
//
//	public <D extends CouchDbDoc> void bulkDocs(JsArray<D> docs, IDocUpdateHandler callback);
//
//	public <D extends CouchDbDoc> void bulkDocs(List<D> docs);
//
//	public <D extends CouchDbDoc> void bulkDocs(List<D> docs, IDocUpdateHandler callback);

	public IChangesFeed changes(ChangesOptions options);

	public void compact(ICompactionHandler callback);

	public void get(String id, IDocGetHandler<?> callback);

	public void get(String id, IDocGetHandler<?> callback, GetDocOptions options);

	public void info(IDbInfoHandler callback);

	public void post(CouchDbDoc doc, IDocCreateHandler callback);

	public void put(CouchDbDoc doc, IDocUpdateHandler callback);

	public void delete(CouchDbDoc doc, IDocUpdateHandler callback);

	public void queryView(String view, ViewQueryOptions options, IViewQueryResultHandler<?> callback);

	public void queryView(String view, IViewQueryResultHandler<?> callback);

	public void queryView(String view, IViewQueryMixedResultHandler callback);
	
	public void queryView(String view, ViewQueryOptions options, IViewQueryMixedResultHandler callback);
	
	public <D extends JavaScriptObject> List<D> arrayToList(JsArray<D> array);

	public <D extends JavaScriptObject> List<D> responseToList(QueryResponse<D> response);
}
