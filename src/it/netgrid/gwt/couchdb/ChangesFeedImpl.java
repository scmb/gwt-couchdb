package it.netgrid.gwt.couchdb;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import it.netgrid.gwt.couchdb.handler.IChangeCompleteHandler;
import it.netgrid.gwt.couchdb.handler.IDocChangeEventHandler;
import it.netgrid.gwt.couchdb.options.ChangesOptions;
import it.netgrid.gwt.couchdb.response.ChangesResponse;
import it.netgrid.gwt.couchdb.response.DocChangeEvent;
import it.netgrid.gwt.couchdb.response.ErrorResponse;

public class ChangesFeedImpl implements IChangesFeed {

	private final List<IChangeCompleteHandler> completeListeners;
	private final List<IDocChangeEventHandler> changeListeners;
	
	private int since;
	private boolean sinceNow;
	private boolean continuous;
	private IConnectionConfig config;
	private ChangesOptions options;
	
	private Request currentRequest;
	private boolean canceled;
	
	public ChangesFeedImpl(IConnectionConfig config, ChangesOptions options) {
		this.changeListeners = new ArrayList<IDocChangeEventHandler>();
		this.completeListeners = new ArrayList<IChangeCompleteHandler>();
		this.since = 0;
		this.sinceNow = false;
		this.continuous = false;
		this.canceled = false;
		this.config = config;
		this.loadOptions(options);
	}
	
	private void loadOptions(ChangesOptions options) {
		if(options == null) return;

		this.options = options;
		this.continuous = options.isContinuous();
		
		if(options.isSinceNow()) {
			sinceNow = true;
		} else {
			since = options.getSince();
		}
	}
	
	@Override
	public void start() {
		this.canceled = false;
		this.sendRequest();
	}
	
	private void sendRequest() {
		if(this.canceled) return;
		UrlBuilder url = initBuilder(config);
		Path path = initPath(config);
		path.add("_changes");
		url.setPath(path.buildPath());
		if(!this.sinceNow) {
			options.setSince(this.since);
		}
		HttpUtils.loadParams(url, options);		
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url.buildString());
		builder.setCallback(this.buildCallback());
		
		builder.setHeader("Accept", "application/json");
		
		try {
			this.currentRequest = builder.send();
		} catch (RequestException e) {
			ErrorResponse error = JavaScriptObject.createObject().cast();
			error.setStatus(Response.SC_SERVICE_UNAVAILABLE);
			error.setError("Request Exception");
			error.setReason(e.getMessage());
			triggerError(error);
		}
	}
	
	private void triggerChange(DocChangeEvent change) {
		for(IDocChangeEventHandler handler : this.changeListeners) {
			handler.onChange(change);

		}
	}
	
	private void triggerCompleted(JsArray<DocChangeEvent> result) {
		for(IChangeCompleteHandler handler : this.completeListeners) {
			handler.onSuccess(result);
		}
	}
	
	private void triggerError(ErrorResponse error) {
		for(IChangeCompleteHandler handler : this.completeListeners) {
			handler.onError(error);
		}
	}

	@Override
	public void cancel() {
		this.canceled = true;
		if(this.currentRequest != null) {
			this.currentRequest.cancel();
		}
	}

	@Override
	public void addChangeListener(IDocChangeEventHandler handler) {
		this.changeListeners.add(handler);
	}

	@Override
	public void removeChangeListener(IDocChangeEventHandler handler) {
		this.changeListeners.remove(handler);
	}

	@Override
	public void addCompleteListener(IChangeCompleteHandler handler) {
		this.completeListeners.add(handler);
	}

	@Override
	public void removeCompleteListener(IChangeCompleteHandler handler) {
		this.completeListeners.remove(handler);
	}
	
	private RequestCallback buildCallback() {
		return new RequestCallback() {
			
			@Override
			public void onResponseReceived(Request request, Response response) {
				switch(response.getStatusCode()) {
				case Response.SC_ACCEPTED:
				case Response.SC_OK:
				case Response.SC_NOT_MODIFIED:
				case Response.SC_CONFLICT:
				case Response.SC_CREATED:
				case Response.SC_NO_CONTENT:
					// Le operazioni per questo genere di risultati verranno
					// eseguite DOPO lo switch che di default ritorna prima
					break;
					
// 				Commentati per evitare di fare troppi confronti ad ogni richiesta
//				Rimangono come indicazione degli altri possibili valori
//
//				case Response.SC_BAD_GATEWAY:
//				case Response.SC_BAD_REQUEST:
//				case Response.SC_CONFLICT:
//				case Response.SC_CONTINUE:
//				case Response.SC_CREATED:
//				case Response.SC_EXPECTATION_FAILED:
//				case Response.SC_FORBIDDEN:
//				case Response.SC_GATEWAY_TIMEOUT:
//				case Response.SC_GONE:
//				case Response.SC_HTTP_VERSION_NOT_SUPPORTED:
//				case Response.SC_INTERNAL_SERVER_ERROR:
//				case Response.SC_LENGTH_REQUIRED:
//				case Response.SC_METHOD_NOT_ALLOWED:
//				case Response.SC_MOVED_PERMANENTLY:
//				case Response.SC_MOVED_TEMPORARILY:
//				case Response.SC_MULTIPLE_CHOICES:
//				case Response.SC_NO_CONTENT:
//				case Response.SC_NON_AUTHORITATIVE_INFORMATION:
//				case Response.SC_NOT_ACCEPTABLE:
//				case Response.SC_NOT_FOUND:
//				case Response.SC_NOT_IMPLEMENTED:
//				case Response.SC_PARTIAL_CONTENT:
//				case Response.SC_PAYMENT_REQUIRED:
//				case Response.SC_PRECONDITION_FAILED:
//				case Response.SC_PROXY_AUTHENTICATION_REQUIRED:
//				case Response.SC_REQUEST_ENTITY_TOO_LARGE:
//				case Response.SC_REQUESTED_RANGE_NOT_SATISFIABLE:
//				case Response.SC_RESET_CONTENT:
//				case Response.SC_SEE_OTHER:
//				case Response.SC_SERVICE_UNAVAILABLE:
//				case Response.SC_SWITCHING_PROTOCOLS:
//				case Response.SC_TEMPORARY_REDIRECT:
//				case Response.SC_UNAUTHORIZED:
//				case Response.SC_UNSUPPORTED_MEDIA_TYPE:
//				case Response.SC_USE_PROXY:
				default:
					ErrorResponse error = JavaScriptObject.createObject().cast();
					error.setStatus(response.getStatusCode());
					error.setError("Error " + response.getStatusCode());
					error.setReason(response.getStatusText());
					triggerError(error);
					return;
				}
				
				JavaScriptObject jsonData = toJsObject(response.getText());
				if(jsonData == null) {
					ErrorResponse error = JavaScriptObject.createObject().cast();
					error.setStatus(response.getStatusCode());
					error.setError("Empty response with status " + response.getStatusCode());
					error.setReason(response.getStatusText());
					triggerError(error);
					return;	
				}
				
				ChangesResponse changesList = jsonData.cast();
				since = changesList.getLastSeq();
				sinceNow = false;
				options.setSince(since);
				for(int i=0; i<changesList.getResults().length(); i++) {
					triggerChange(changesList.getResults().get(i));
				}
				
				if(continuous)
					sendRequest();
				else
					triggerCompleted(changesList.getResults());
			}
			
			@Override
			public void onError(Request request, Throwable exception) {
				ErrorResponse error = JavaScriptObject.createObject().cast();
				error.setStatus(Response.SC_SERVICE_UNAVAILABLE);
				error.setError("Request error");
				error.setReason(exception.getMessage());
				triggerError(error);
			}
		};
	}

	private static final JavaScriptObject toJsObject(String text) {
		if(text == null || text.equals("")) return null;
		
		JSONValue value = JSONParser.parseStrict(text);
		return value.isObject() == null ? null : value.isObject().getJavaScriptObject();
	}
	
	private static final native boolean isDbError(JavaScriptObject obj) /*-{
		if(obj == null) return true;
		
		if(typeof obj._id != "undefined")
			return false;
		else 
			return typeof obj.error != "undefined";
	}-*/;
	
	private static UrlBuilder initBuilder(IConnectionConfig config) {
		UrlBuilder builder = new UrlBuilder();
		
		builder.setProtocol(config.getProtocol().name());
		builder.setHost(config.getHostname());
		builder.setPort(config.getPort());
		
		return builder;
	}
	
	private static Path initPath(IConnectionConfig config) {
		Path path = new Path();
		path.add(config.getDbName());
		return path;
	}
	
	private static String connString(IConnectionConfig config) {
		UrlBuilder builder = new UrlBuilder();
		
		builder.setProtocol(config.getProtocol().name());
		builder.setHost(config.getHostname());
		builder.setPort(config.getPort());
		builder.setPath(config.getDbName());
		
		return builder.buildString();
	}
}