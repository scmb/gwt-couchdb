package it.netgrid.gwt.couchdb;

import java.util.ArrayList;

public class Path extends ArrayList<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8218355266916462754L;

	public static final String SEPARATOR = "/";
	
	public static Path parse(String path) {
		Path retval = new Path();
		
		if(path == null) return retval;
		
		String[] parts = path.trim().split(SEPARATOR);
		
		for(int i=0; i < parts.length; i++) {
			retval.add(parts[i]);
		}
		
		return retval;
	}
	
	public String buildPath() {
		String retval = "";
		String glue = "";
		
		for(String item : this) {
			retval += glue + item;
			glue = SEPARATOR;
		}
		
		return retval;
	}
	
	// Tipicamente il to string viene utilizzato quando si concatena l'elemento ad altri
	// Per questo in questo caso viene aggiunto già il separatore mentre il buildpath No
	@Override
	public String toString() {
		return SEPARATOR + this.buildPath() + SEPARATOR;
	}
	
}
