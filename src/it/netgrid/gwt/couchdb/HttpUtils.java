package it.netgrid.gwt.couchdb;

import java.util.logging.Logger;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.json.client.JSONObject;

public final class HttpUtils {
	
	public static Logger log = Logger.getLogger("HttpUtils");
	
	public static final String getRequestUrl(IConnectionConfig config, CouchDbDoc doc, JavaScriptObject options, JsArrayString blacklist) {
		String id = doc == null ? "" : doc.getId();
		UrlBuilder builder = new UrlBuilder();
		
		builder.setProtocol(config.getProtocol().name());
		builder.setHost(config.getHostname());
		builder.setPort(config.getPort());
		builder.setPath(config.getDbName() + id == null ? "" : "/" + id);
		
		if(options != null) {
			loadParams(builder, options, blacklist == null ? getEmptyCleanMask() : blacklist);
		}
		
		return builder.buildString();
	}
	
	public static final void loadParams(UrlBuilder builder, JavaScriptObject obj) {
		loadParams(builder, obj, getEmptyCleanMask());
	}
	
	public static final native void loadParams(UrlBuilder builder, JavaScriptObject obj, JsArrayString blacklist) /*-{
		if(obj == null) return;
		var str = [];
		for ( var p in obj) {
			if (obj.hasOwnProperty(p) && blacklist.indexOf(p) == -1 && typeof obj[p] != "function") {
				var value = obj[p];
				if(Object.prototype.toString.call( value ) === '[object Array]') {
					value = JSON.stringify(value);
					@it.netgrid.gwt.couchdb.HttpUtils::setParameter(Lcom/google/gwt/http/client/UrlBuilder;Ljava/lang/String;Ljava/lang/String;)(builder, p, value);
				} else if(typeof value == "string"){
					@it.netgrid.gwt.couchdb.HttpUtils::setParameter(Lcom/google/gwt/http/client/UrlBuilder;Ljava/lang/String;Ljava/lang/String;)(builder, p, value);
				} else if(typeof value == "object") {
					@it.netgrid.gwt.couchdb.HttpUtils::loadParams(Lcom/google/gwt/http/client/UrlBuilder;Lcom/google/gwt/core/client/JavaScriptObject;Lcom/google/gwt/core/client/JsArrayString;)(builder,value,blacklist);
				} else {
					value = JSON.stringify(value);
					@it.netgrid.gwt.couchdb.HttpUtils::setParameter(Lcom/google/gwt/http/client/UrlBuilder;Ljava/lang/String;Ljava/lang/String;)(builder, p, value + "");
				}
			}
		}
	}-*/;
	
	public static final void setParameter(UrlBuilder builder, String name, String value) {
		builder.setParameter(name, value);
	}
	
	public static final void loadData(RequestBuilder builder, CouchDbDoc doc) {
		builder.setRequestData(new JSONObject(doc).toString());
	}

	
	private static final native JsArrayString getEmptyCleanMask() /*-{
		return [];
	}-*/;

	private static final native JsArrayString getDocCleanMask() /*-{
		return ["_id"];
	}-*/;

	
	public static final native CouchDbDoc cleanClone(CouchDbDoc doc, JsArrayString remove) /*-{
		var retval = doc;
		if(remove == null)
			return retval;
		
		for(var i = 0; i < remove.length; i++) {
			delete retval[remove[i]];
		}
		
		return retval
	}-*/;

	
	public static final native JsArrayString getAttributes(JavaScriptObject options) /*-{
		var keys = [];
		for(var key in options) {
			if (options.hasOwnProperty(p)) {
				keys.push(key);
			}
		}
		return keys;
	}-*/;
	
	public static final native String asQuery(JavaScriptObject options) /*-{
		var str = [];
		for ( var p in obj)
			if (obj.hasOwnProperty(p) && typeof obj[p] != "function") {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
		return str.join("&");
	}-*/;

	public static final native String asQuery(JavaScriptObject options, JsArrayString blacklist) /*-{
		var str = [];
		for ( var p in obj)
			if (obj.hasOwnProperty(p) && list.indexOf(p) != -1) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
		return str.join("&");
	}-*/;

}
